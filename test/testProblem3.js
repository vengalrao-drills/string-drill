
// ==== String Problem #3 ====
// Given a string in the format of "20/1/2021", print the month in which the date is present in.

const dateFormat = require('../problem3')  // import the dateFormat function
console.log(dateFormat("20/1/2021"))   // output :-   month :- 20  date :- 1
console.log(dateFormat("25/11/2001"))  // output :-   month :- 25  date :- 11