
// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.


const checkIp = require('../problem2')
console.log(checkIp("111.255.161.143"));    // output - [ '111', '255', '161', '143' ]
console.log(checkIp("111.255.161"));        // output - []
console.log(checkIp("111.255.161.256"));    // output - []
console.log(checkIp("111.255.256.143"));    // output - []