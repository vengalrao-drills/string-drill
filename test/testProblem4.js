
// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}


const makeCap = require('../problem4')   // importing the makeCap function
console.log(makeCap({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMiTh"}))  // output :-  { first_name: 'John', middle_name: 'Doe', last_name: 'Smith' }
console.log(makeCap({"first_name": "JoHN", "last_name": "SMith"}))  // output :-  { first_name: 'John', last_name: 'Smith' }