// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function makeCapital(val) {
  // return val.slice(0,1).toUpperCase() + val.slice(1).toLowerCase()
  return val.substring(0, 1).toUpperCase() + val.slice(1).toLowerCase(); // making the first letter capital and rest all small letters.
  // takking first letter as capital with the help of slice and toUpperCase() and adding rest small letters from index 1 to the end using toLowerCase() function.
}

function makeCap(info) {
  // object received in the form of info
  for (let i in info) {
    //  iterating through the object.
    info[i] = makeCapital(info[i]); // making the values of object capital. to make it. we are sending through a function. it returns a capitalize value/]. and changes made into the original object.
  }
  return info; // returing  the info value
}

module.exports = makeCap; // exporting the function to other modules
