// ==== String Problem #3 ====
// Given a string in the format of "20/1/2021", print the month in which the date is present in.

function dateFormat(dataVal) {
  // date recieved in the form of dataVal
  let data = dataVal.split("/"); // splitting the date
  return `month :- ${data[0]}  date :- ${data[1]}`; // returning the month and date
}

module.exports = dateFormat; // exporting the function to other modules
