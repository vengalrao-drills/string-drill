
// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string. 

function makeSentence(info){
    return info.join(' ')+'.'   // we need to join the all the words in an array with space and add a '.'
}

module.exports = makeSentence;  // exporting the function to other modules