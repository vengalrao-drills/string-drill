// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function checkIp(ipAdd) {
  // input received ipAddress in the form of ipAdd
  let address = ipAdd.split("."); // making it into an array by splitting '.'
  if (address.length != 4) {
    // if my address is less than length 4 - then, i can direclty say it's not a valid ipv4 address
    return []; // so returing a empty array
  }
  for (let i of address) {
    // now iterating through the ipAddress
    if (Number(i) > 255) {
      // if it is greater than 255 then it's invalid ipv4 address
      return []; // then return empty array
    }
  }
  return address; // if it satisfies all the statement above then return address
}


module.exports = checkIp;  // exporting the function to other modules
