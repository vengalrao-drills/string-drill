// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss -100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0.

function convertNum(num) {
  let check = num.replace(/[^a-zA-Z]/g, "");  // here i am checking whether any alphabet is present in the given input.

  if (check) {  // if any character is present. then i am returing directly 0,  then the function ends
    return 0;
  } else {  // else if no alphabets then there will be only letter inside this
    let newNumber = num.replace(/[^0-9.-]/g, ""); //here, it will remove all the characters except from '0-9' , '.' , '-' and storing in a newNumber.
    // i need to remove the dollar symbol and commans and any extra alphabets- to convert it into a number.
 
    return parseFloat(newNumber)   
  } 
}

module.exports = convertNum;  // exporting the function to other modules
